Funcion fact <- facto (x)
	Si x=1 O x=0 Entonces
		fact <- 1
	SiNo
		c <- 1
		Para v<-1 Hasta x Hacer
			c <- c*v
		FinPara
		fact <- c
	FinSi
FinFuncion

Algoritmo Ejercicio1
	Escribir 'ingrese un valor para x: '
	Leer x
	Escribir 'ingrese la cantidad de cifras significativas(entre 1 y 12): '
	Leer cifras
	Ess <- 0.5*(10^(2-cifras))
	ite <- 0
	result <- 0
	resultAn <- 0
	Ea <- 100000000
	Mientras Ea>Ess Hacer
		Si result=0 Entonces
			result <- result+(((-1)^ite)/facto(2*ite+1))*(x^(2*ite+1))
			ite <- ite+1
		SiNo
			resultAn <- result
			result <- result+(((-1)^ite)/facto(2*ite+1))*(x^(2*ite+1))
			ite <- ite+1
			Ea <- abs((result-resultAn)/result)*100
		FinSi
	FinMientras
	Escribir 'el resultado de la funcion sen x es: ',result
	Escribir 'el error aproximado es: ',Ea
	val <- sen(x)
	Escribir 'valor real: ',val
	Er <- val-result
	Err <- Er/val
	Ep <- Err*100
	Escribir 'el error cometido es: ',Ep
FinAlgoritmo
