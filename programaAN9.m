%funcion de arctg x = x - (x^3)/3 + (x^5)/5 ...

x= input('Ingrese el valor de x (-1<x<1) ');
while(x<=-1 || x>=1)
    x= input('Ingrese el valor de x (-1<x<1) ');
end

cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 15): ');

Es = 0.5*(10^(2-cifras));
ite = 0;
resultado = 0;
resulAnt = 0;
Ea = 100000000;

while(Ea > Es)
    resulAnt=resultado;
    resultado=resultado+((((-1)^ite)*(x^(2*ite+1)))/(2*ite+1));
    Ea=abs(resultado-resulAnt)/abs(resultado)*100;
    ite=ite+1;
end

fprintf('\nel resultado de la funcion arctg(%g) es: %f\n', x, resultado);

fprintf('el error aproximado es: %f\n', Ea);

valor=atan(x);
fprintf('el valor real es: %f\n', valor);

Er=abs(valor-resultado);
fprintf('Er = %d\n', Er);

Err=Er/abs(valor);
fprintf('Err = %d\n', Err);

Ep=Err*100;
fprintf('Ep = %d\n', Ep);