%funcion de arcsen x = x + 1(x^3)/2*3 +1*3(x^5)/2*4*5 ...

x= input('Ingrese el valor de x (-1<x<1) ');
while(x<=-1 || x>=1)
    x= input('Ingrese el valor de x (-1<x<1) ');
end

cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 15): ');

Es = 0.5*(10^(2-cifras));
ite = 0;
resultado = 0;
resulAnt = 0;
Ea = 100000000;

while (Ea>Es)
resulAnt=resultado;
resultado=resultado+((factorial(2*ite)/((2^ite)*factorial(ite))^2)*((x^(2*ite+1))/(2*ite+1)));
Ea=abs(resultado-resulAnt)/abs(resultado)*100;
ite=ite+1;
end

fprintf('\nel resultado de la funcion arcsen(%g) es: %f\n', x, resultado);

fprintf('el error aproximado es: %f\n', Ea);

valor=asin(x);
fprintf('el valor real es: %f\n', valor);

Er=abs(valor-resultado);
fprintf('Er = %d\n', Er);

Err=Er/abs(valor);
fprintf('Err = %d\n', Err);

Ep=Err*100;
fprintf('Ep = %d\n', Ep);
