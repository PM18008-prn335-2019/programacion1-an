
%Programa Analisis Numerico n�1

fprintf('Bienvenido a Analisis Numerico\nAcontinuacion te mostraremos un menu donde podras elegir cualquier serie de 9\nque desees ejecutar para una funcion resuelta de forma iterativa.\n');

fprintf('opciones:\n1.sen x\n2.cos x\n3.e^x\n4.sh x\n5.ch x\n6.arcsen x\n7.ln(1+x)\n8.1/(1+x^2)\n9.arctg x\nOtro para salir.\n');
opcion=input('Ingrese una opcion: ');

while (opcion>=1 && opcion<=9)
  switch (opcion)
    case 1
      %funcion de sen x = x - (x^3)/3! +(x^5)/5!....

        x = input('ingrese un valor para x: ');

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 0;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while (Ea > Es)
          if (resultado == 0)
            resultado = resultado + (((-1)^ite)/factorial((2*ite + 1)))*(x^(2*ite +1));
            ite = ite+1;
          else 
            resulAnt = resultado;
            resultado = resultado + (((-1)^ite)/factorial((2*ite + 1)))*(x^(2*ite +1));
            ite = ite+1;
            Ea = abs((resultado - resulAnt)/resultado)*100;
          end
        end

        fprintf('\nel resultado de la funcion sen(%g) es: %f\n', x, resultado );

        fprintf('el error aproximado es: %f\n', Ea);

        valor=sin(x);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d %\n', Ep);
      
    case 2
      %funcion de cos x = 1 - (x^2)/2! +(x^4)/4!-....

        x = input('ingrese un valor para x: ');

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 0;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while (Ea > Es)
          if (resultado == 0)
            resultado = resultado + (((-1)^ite)/factorial((2*ite)))*(x^(2*ite));
            ite = ite+1;
          else 
            resulAnt = resultado;
            resultado = resultado + (((-1)^ite)/factorial((2*ite)))*(x^(2*ite));
            ite = ite+1;
            Ea = abs((resultado - resulAnt)/resultado)*100;
          end
        end

        fprintf('\nel resultado de la funcion cos(%g) es: %f\n', x, resultado);

        fprintf('el error aproximado es: %f\n', Ea);

        valor=cos(x);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d\n', Ep);
    case 3
      %funcion de e^x = 1 + (x)/1! +(x^2)/2!....

        x = input('ingrese un valor para x: ');

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 0;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while (Ea > Es)
          if (resultado == 0)
            resultado = resultado + ((x)^ite)/factorial(ite);
            ite = ite+1;
          else 
            resulAnt = resultado;
            resultado = resultado + ((x)^ite)/factorial(ite);
            ite = ite+1;
            Ea = abs((resultado - resulAnt)/resultado)*100;
          end
        end

        fprintf('\nel resultado de la funcion e^(%g) es: %f\n', x, resultado);

        fprintf('el error aproximado es: %f\n', Ea);

        valor=exp(x);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d\n', Ep);
    case 4
      %funcion de senh x = x + (x^3)/3! +(x^5)/5!....

        x = input('ingrese un valor para x: ');

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 0;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while (Ea > Es)
          if (resultado == 0)
            resultado = resultado + ((1)/factorial((2*ite + 1)))*(x^(2*ite +1));
            ite = ite+1;
          else 
            resulAnt = resultado;
            resultado = resultado + ((1)/factorial((2*ite + 1)))*(x^(2*ite +1));
            ite = ite+1;
            Ea = abs((resultado - resulAnt)/resultado)*100;
          end
        end

        fprintf('\nel resultado de la funcion senh(%g) es: %f\n', x, resultado);

        fprintf('el error aproximado es: %f\n', Ea);

        valor=sinh(x);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d\n', Ep); 
    case 5
      %funcion de ch x = 1 + (x^2)/2! +(x^4)/4! ...
 
        x= input('Ingrese el valor de x ');

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 0;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while (Ea>Es)
            resulAnt=resultado;
            resultado=resultado+((x^(2*ite))/factorial(2*ite));
            Ea=abs(resultado-resulAnt)/abs(resultado)*100;
            ite=ite+1;
        end

        fprintf('\nel resultado de la funcion ch(%g) es: %f\n', x, resultado);

        fprintf('el error aproximado es: %f\n', Ea);

        valor=cosh(x);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d\n', Ep);
    case 6
      %funcion de arcsen x = x + 1(x^3)/2*3 +1*3(x^5)/2*4*5 ...

        x= input('Ingrese el valor de x (-1<x<1) ');
        while(x<=-1 || x>=1)
            x= input('Ingrese el valor de x (-1<x<1) ');
        end

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 0;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while (Ea>Es)
        resulAnt=resultado;
        resultado=resultado+((factorial(2*ite)/((2^ite)*factorial(ite))^2)*((x^(2*ite+1))/(2*ite+1)));
        Ea=abs(resultado-resulAnt)/abs(resultado)*100;
        ite=ite+1;
        end

        fprintf('\nel resultado de la funcion arcsen(%g) es: %f\n', x, resultado);

        fprintf('el error aproximado es: %f\n', Ea);

        valor=asin(x);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d\n', Ep);
    case 7
      %funcion de ln(1 + x) = x - (x^2)/2 + (x^3)/3 ...

        x= input('Ingrese el valor de x (-1<x<1) ');
        while(x<=-1 || x>=1)
            x= input('Ingrese el valor de x (-1<x<1) ');
        end

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 1;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while (Ea > Es)
            resulAnt=resultado;
            resultado=resultado+(((-1)^(ite-1))/ite*(x^ite));
            Ea=abs(resultado-resulAnt)/abs(resultado)*100;
            ite=ite+1;

        end
        fprintf('\nel resultado de la funcion ln(1 + %g) es: %f\n', x, resultado);

        fprintf('el error aproximado es: %f\n', Ea);

        valor=log(1+x);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d\n', Ep);
    case 8
      %funcion de 1/(1 + x^2) = 1 - (x^2) + (x^4) ...

        x= input('Ingrese el valor de x (-1<x<1) ');
        while(x<=-1 || x>=1)
            x= input('Ingrese el valor de x (-1<x<1) ');
        end

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 0;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while (Ea > Es)
            resulAnt=resultado;
            resultado=resultado+(((-1)^ite)*(x^(2*ite)));
            Ea=abs(resultado-resulAnt)/abs(resultado)*100;
            ite=ite+1;
            if(ite==49)
            break
            end
        end

        fprintf('\nel resultado de la funcion 1/(1 + (%g)^2) es: %f\n', x, resultado);

        fprintf('el error aproximado es: %f\n', Ea);

        valor=1/(1+x^2);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d\n', Ep);
    case 9
      %funcion de arctg x = x - (x^3)/3 + (x^5)/5 ...

        x= input('Ingrese el valor de x (-1<x<1) ');
        while(x<=-1 || x>=1)
            x= input('Ingrese el valor de x (-1<x<1) ');
        end

        cifras = input('ingrese con cuantas cifras significativas desea la respuesta(no mayor a 12): ');

        Es = 0.5*(10^(2-cifras));
        ite = 0;
        resultado = 0;
        resulAnt = 0;
        Ea = 100000000;

        while(Ea > Es)
            resulAnt=resultado;
            resultado=resultado+((((-1)^ite)*(x^(2*ite+1)))/(2*ite+1));
            Ea=abs(resultado-resulAnt)/abs(resultado)*100;
            ite=ite+1;
        end

        fprintf('\nel resultado de la funcion arctg(%g) es: %f\n', x, resultado);

        fprintf('el error aproximado es: %f\n', Ea);

        valor=atan(x);
        fprintf('el valor real es: %f\n', valor);

        Er=abs(valor-resultado);
        fprintf('Er = %d\n', Er);

        Err=Er/abs(valor);
        fprintf('Err = %d\n', Err);

        Ep=Err*100;
        fprintf('Ep = %d\n', Ep);
  end
  
  continuar=input('Desea continuar 1.Si Otro salir ');
  if(continuar==1)
    fprintf('\nopciones:\n1.sen x\n2.cos x\n3.e^x\n4.sh x\n5.ch x\n6.arcsen x\n7.ln(1+x)\n8.1/(1+x^2)\n9.arctg x\nOtro para salir.\n');
    opcion=input('Ingrese una opcion: ');
  else
    opcion=33;
  end
end